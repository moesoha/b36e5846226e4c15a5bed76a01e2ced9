function getQueryParams(search) {
	var params = {};
	search
		.split('&')
		.map(function (s) { return s.split('='); })
		.map(function (a) { return [a.shift(), a.join('=')].map(function (s) { return decodeURIComponent(s || ''); }); })
		.filter(function (a) { return a[0].length > 0; })
		.forEach(function (a) { params[a[0]] = a[1]; })
	;
	return params;
}

// window.$_GET = getQueryParams(window.location.search.slice(1));