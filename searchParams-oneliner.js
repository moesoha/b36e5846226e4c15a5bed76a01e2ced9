// compatibility: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/fromEntries
const getQueryParams = s => Object.fromEntries(s.split('&').map(s => s.split('=')).map(a => [a.shift(), a.join('=')].map(s => decodeURIComponent(s || ''))).filter(([k, _]) => k.length > 0));
// window.$_GET = getQueryParams(window.location.search.slice(1));